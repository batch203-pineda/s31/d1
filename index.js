// Use the "require" directive to load Node.js Modules
    // a "module" is a software component or part of a program that contains one or more routines.
// "http module" let's Node.js to transfer data using the Hpertext Transfer Protocol.
    // This is a set of individual files that contain code to create a "component" that helps establish data transfer between application.
    // allows us to fetch resources such as HTML documents.
//  Client (browser) and servers (nodeJS/expressJS application) communicate by exchanging individual messages (requests/response).
// The message sent by the client is call "request".
// The message sent by the server is an answer is called "response".

    let http = require("http"); 

// Using this module's createServer() method, we can create an HTTP server that listens to the requests on a specified port and gives responses back to the client.

// A port is a virtual point where networks connections starts and end.
// Each port is associated with specific process or services.
// The server will br assigned to port 4000 via the "listen()" method where the server will listen to any request that are sent to it and will also send the response via this port.

http.createServer(function(request,response){
    // Use to the writeHead() method to:
        // Set a status code for the response. (200 means "OK")
        // Set the content-type of the response. (plain text message)
    response.writeHead(200,{"Content-Type": "text/plain"});

    response.end("Hello World");

    // response.writeHead(200,{"Content-Type": "text/html"});

    // response.end("<h1>Hello Worlds</h1>");
}).listen(4000); 

console.log("Server is running at localhost:4000");

// 3000, 4000, 8000, 5000 - Usually used for web development.
// We will access the server in the browser using the localhost:4000

// kill port "npx kill-port 4000"
// 

// force install nodemon liveserver: sudo npm install -g nodemon

